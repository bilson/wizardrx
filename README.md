# WizardRX

This repository contains the hardware and firmware for the WizardRX, a low-cost
6 channel 5.8GHz video receiver. It can be hand-soldered easily using cheap
parts found online, costing roughly £60-70.

![](images/pcb_v1_diy.png)

Channels can be set through a three-button interface, with an OLED display for
feedback. There is also voltage monitoring to make sure your battery doesn't
explode.

## Motiviation

We wanted to be able to view video feeds at our race events. Buying six
standalone receivers cost too much (roughly £120 or more, double that if you
want diversity) and take up too much space. This seemed like a better way
forward.


## Status

The first boards have been ordered and have not yet arrived, so all of this is
currently __untested__. Proceed at your own risk!

Work on the firmware will start once the first board is put together.


# Hardware

The KiCad and Gerber files can be found under `hardware/`.

## Parts List

- (6x) RX5808 modules (with SPI resistor mod)
- (6x) SMA (or RP-SMA) pigtail extensions.
- (1x) Arduino Nano
- (1x) 5V regulator
- (1x) 3.3V regulator
- (1x) 100k resistor and (1x) 10k resistor
- (1x) 1N5817 Schottky diode (or similiar)


# Firmware

The firmware is yet to be developed.


# License

The project is licensed under the terms of the MIT license.
